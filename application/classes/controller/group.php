<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Group extends Controller {

    public function action_index() {

        $user = ORM::factory('user', 1);
        
        echo "<h2>User</h2>";
        
        $ownerOfGroup = $user->ownerofGroups->find_all();
        
        foreach($ownerOfGroup as $group) {
        	print_r("Owner of group: " . $group->name . "<br />");
        }
        
        echo "<br />";
    	echo "<h3>Groups of the User</h3>";
       	$groups = $user->groups->find_all();
       	
       	foreach($groups as $group) {
       		
       		print_r("Group: " . $group->name . "<br />");
       		
       		foreach($group->albums->find_all() as $album) {
       			print_r("Album: " . $album->name . "<br />");
       		}
       	}
   	
       	echo "<br />";
       	echo "<h3>Medias of the User</h3>";
       	$medias = $user->medias->find_all();
       	
       	foreach($medias as $media) {
       		
       		print_r($media->name . " is an " . $media->mediatype->name . "<br />");

       		foreach($media->comments->find_all() as $comment) {
       			print_r("Comment: " . $comment->text . "<br />");
       		}
       		
       		foreach($media->tags->find_all() as $tag) {
       			print_r("Tag: " . $tag->name . "<br />");
       		}
  
       		foreach($media->groups->find_all() as $group) {
       			print_r("Group: " . $group->name . "<br />");
       		}
       	}
    }
}