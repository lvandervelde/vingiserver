<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller {

	private $error = "";
	
    public function action_index() {

    	if( $this->request->method() == Request::GET ) {
    		
    		$query = json_decode(urldecode($_GET['q']), true);
    		
    		if(json_last_error() == JSON_ERROR_NONE) {
    			
    			$command = $query['command'];
    			
    			switch($command) {
    				
    				case "getById":
    					$this->getById($query);
    					break;
    				default:
    					$error = "Unknown command";
    					throw new BadFunctionCallException($error);
    			}
    		}
    	} else {
    		#TODO 
    		$error = "Bad Request";
    		throw new Exception($error);
    	}
    }
    
    private function getById($query) {
    	$user = ORM::factory('user', $query['id']);
    	echo $user->to_json();
    }
}