<?php defined('SYSPATH') or die('No direct script access.');

class Model_Groupmedia extends ORM
{		
	
	protected $_belongs_to = array('group' => array('foreign_key' => 'id'), 'media' => array('foreign_key' => 'id'));
	
}