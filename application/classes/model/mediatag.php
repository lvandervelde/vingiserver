<?php defined('SYSPATH') or die('No direct script access.');

class Model_Mediatag extends ORM
{		
	
	protected $_belongs_to = array('media' => array('foreign_key' => 'id'), 'tag' => array('foreign_key' => 'id'));
	
}