<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tag extends ORM
{
	protected $_table_name = 'tag';
	protected $_primary_key = 'id';
	
	protected $_has_many = array(
									'medias' => array(
														'model' => 'media',
														'far_key' => 'mediaId',
														'foreign_key' => 'tagId',
														'through' => 'mediatag'
											)
							);
}