<?php defined('SYSPATH') or die('No direct script access.');

class Model_Group extends ORM
{
	protected $_table_name = 'group';
	protected $_primary_key = 'id';
	
	protected $_belongs_to = array('user' => array('foreign' => 'ownerId'));
	
	protected $_has_many = array(
									'albums' 	=> array(
															'foreign_key' => 'groupId', 
									),
									'users' 	=> array(
															'model' => 'user', 
															'far_key' => 'userId', 
															'foreign_key' => 'groupId', 
															'through' => 'usergroup'
									),
									'medias' 	=> array(
															'model' => 'media',
															'far_key' => 'mediaId',
															'foreign_key' => 'groupId',
															'through' => 'groupmedia'
									)
								);	
}