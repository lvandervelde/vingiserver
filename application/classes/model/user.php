<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends ORM
{
	protected $_table_name = 'user';
	protected $_primary_key = 'id';
	
	protected $_has_many = array(
									'ownerofGroups' => array(
														'model' => 'group',
														'foreign_key' => 'ownerId'
									
									),
									'groups' => array(
														'model' => 'group', 
														'far_key' => 'groupId', 
														'foreign_key' => 'userId', 
														'through' => 'usergroup'
									),
									'medias' => array(
														'model' => 'media',
														'foreign_key' => 'userId'		
									)
							);
	
	public function to_json() {
		$assoc_array = array( 
								'firstname' => $this->firstname,
								'lastname' => $this->lastname,
								'password' => $this->password,
								'email' => $this->email,
								'userUnique' => $this->userUnique,
								'phonenumber' => $this->phonenumber,
								'registerDate' => $this->registerDate,
								'sessionId' => $this->sessionId 
							);
		
		return json_encode($assoc_array);
	}
	
}