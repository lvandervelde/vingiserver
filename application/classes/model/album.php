<?php defined('SYSPATH') or die('No direct script access.');

class Model_Album extends ORM
{
	
	protected $_primary_key = 'id';
	protected $_table_name = 'album';

	protected $_belongs_to = array('group' => array('foreign_key' => 'groupId'));
	
}