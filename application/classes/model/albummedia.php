<?php defined('SYSPATH') or die('No direct script access.');

class Model_Albummedia extends ORM
{		
	
	protected $_belongs_to = array('album' => array('foreign_key' => 'id'), 'media' => array('foreign_key' => 'id'));
	
}