<?php defined('SYSPATH') or die('No direct script access.');

class Model_Media extends ORM
{
	protected $_table_name = 'media';
	protected $_primary_key = 'id';
	
	protected $_belongs_to = array('mediatype' => array('foreign_key' => 'typeId'), 'user' => array('foreign_key' => 'userId'));
	
	protected $_has_many = array(
									'comments' 	=> array(
															'model' => 'comment', 
															'foreign_key' => 'mediaId'
									),
									'tags' 		=> array(
															'model' => 'tag',
															'far_key' => 'tagId',
															'foreign_key' => 'mediaId',
															'through' => 'mediatag'
									),
									'groups' 	=> array(
															'model' => 'group', 
															'far_key' => 'groupId', 
															'foreign_key' => 'mediaId', 
															'through' => 'groupmedia'
									)	
							);
}